import React from 'react';
import {BootstrapTable,TableHeaderColumn } from 'react-bootstrap-table';

	const ProductTable=(props)=>{
		return (
		<BootstrapTable data={props.rowData} striped hover>
	      <TableHeaderColumn isKey dataField='name'>Product Name</TableHeaderColumn>
	      <TableHeaderColumn dataField='price'>Product Price</TableHeaderColumn>
	      <TableHeaderColumn dataField='stock'>Product Stock</TableHeaderColumn>
	  	  <TableHeaderColumn dataField='delete' dataFormat={props.buttonFunction}>Action</TableHeaderColumn>
	  	</BootstrapTable>
	  	 );
	}
 

export default ProductTable;
