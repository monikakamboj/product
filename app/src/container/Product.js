
import React, { Component } from 'react';
import { Form, Text } from "react-form";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {addProduct,deleteProduct,updateProduct} from '../redux/product';
import Validation from "../utilities/Regex";
import Message from "../utilities/messages";
import ProductTable from './ProductTable';
import AlertMsg from "../components/AlertMsg";
import _ from 'lodash';
class Product extends Component {
	constructor(props){
		super(props);
		this.state={
			name:'',
			price:'',
			stock:'',
			button:'Submit',
			id:'',
			open:false,
			rowDetail:{}
		}
		    this.onFormSubmit = this.onFormSubmit.bind(this);

	}

onFormSubmit(values) {
		
		let productData={
			name:values.productName,
			stock:values.stock,
			price:values.price
		}
		 if(this.state.button == 'Submit'){
		 this.props.addProduct(productData);
		 this.setState({
	          open: true,
	          msg: Message.addDetail,
	          msgType: 'Success',
	          msgStatus:'false',
	        });
		 	this.setState({name:'',price:'',stock:''})
		}
		 else{
		 	 productData.id=this.state.id;
		 	this.props.updateProduct(productData);
		 	 this.setState({
		          open: true,
		          msg: Message.updateDetail,
		          msgType: 'Success',
		          msgStatus:'false',
		        });
		 	this.setState({name:'',price:'',stock:'',button:'Submit'});
		 }
		
	}
	buttonFunction(cell,row){
		console.log(row,'row')
		return(
			<div>
				<i className="fa fa-trash-o" onClick={()=>this.confirmDelete(cell,row)} style={{fontSize:'25px',color:'red'}}></i>
				<i className="fa fa-pencil"  onClick={()=>this.update(row)} style={{fontSize:'25px',color:'red'}}></i>
			</div>)
	}
	/**************  message to confirm delete product ***************/
  confirmDelete(cell,row) {
    this.setState({
      open: true,
      msg: Message.deleteWarning,
      msgType: Message.confirmDelete,
      msgStatus: "warning",
      rowDetail: row
    });
  }
	deleteProductDetail(row){
		this.props.deleteProduct(row);	
		 this.setState({
          open: true,
          msg: Message.message,
          msgType: 'Success',
          msgStatus:'false',
        });
	}
	update(row){
		this.setState({name:row.name,stock:row.stock,price:row.price,button:'Update',id:row.id});
	}
 
  render() {
  	let {name,price,stock} =this.state;
    return (
     <div >  
	 <div>
	   {
          <AlertMsg
            onPress={() => this.setState({ open: false })}
            onDelete={() => this.deleteProductDetail(this.state.rowDetail)}
            isShowingModal={this.state.open}
            msg={this.state.msg}
            type={this.state.msgType}
            status={this.state.msgStatus}
          />
        }

			<div >
			 <Form
            onSubmit={values => {
              this.onFormSubmit(values);
            }}
             validate={({ productName, price, stock }) => {
                        return {
                        productName: !productName
                            ? Message.productNameEmpty
                            : !Validation.validateFreeSpace(productName)
                              ? Message.spaceNotAllowed
                              : undefined,
                        price: !price
                            ? Message.priceEmpty
                            : !Validation.validateFreeSpace(price)
                              ? Message.spaceNotAllowed
                              : undefined,
                        stock: !stock
                            ? Message.stockEmpty
                            : !Validation.validateFreeSpace(stock)
                              ? Message.spaceNotAllowed
                              : undefined,

                        };
                      }}
              defaultValues={{
              	productName:name,
              	price:price,
              	stock:stock,
              }}
          >
            {({ submitForm,reset,setTouched}) => {
              return (
               <form onSubmit={submitForm}>
				<div className="col-md-12">
					<h2>Product</h2>
					<div  className="col-md-6">
						<label >Product Name:</label>
						 <Text
	                          field="productName"
	                          placeholder="* Name of the Product"
	                          className="form-control"
	                          maxLength="50"
	                          onChange={(event)=>this.setState({name:event.target.value})}
	                        />
					</div>	
					<div  className="col-md-6">
						<label >Price:</label>
						 <Text
	                          field="price"
	                          placeholder="*Price"
	                          className="form-control"
	                          maxLength="50"
	                          type="number"
	                          onChange={(event)=>this.setState({price:event.target.value})}
	                        />
					</div>
					<div  className="col-md-6">
						<label >Stock:</label>
						 <Text
	                          field="stock"
	                          placeholder="* Stock"
	                          className="form-control"
	                          maxLength="50"
	                           type="number"
	                          onChange={(event)=>this.setState({stock:event.target.value})}
	                        />
					</div>
					<div className="col-md-6">
					<button className="button button-round button-font-large button-font-text-medium-light button-height-small button-shadow button-width-large" style={{marginTop:25}}>
                               { this.state.button}
                              </button>
					</div>
					</div>
				  </form>
              );
            }}
          </Form>	
			</div>
			<div>
				 <ProductTable rowData={this.props.product} buttonFunction={this.buttonFunction.bind(this)}/>
  			</div>
	</div>
	 </div>
    );
  }
}

const mapSatetToProps = state => {
  return {
    product:state.product
  }
}

const mapDispatchToProps = dispatch => {
  return {
    addProduct:bindActionCreators(addProduct, dispatch),
	deleteProduct:bindActionCreators(deleteProduct, dispatch),
	updateProduct:bindActionCreators(updateProduct,dispatch)

  }
}
export default Product = connect( mapSatetToProps,mapDispatchToProps)(Product);



