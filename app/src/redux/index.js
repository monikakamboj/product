
import product from './product';
import { combineReducers } from 'redux';


const productApp = combineReducers({
  product,
})

export default productApp