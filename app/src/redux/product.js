
import _ from 'lodash';
//Actions 
let productId=0;

export const addProduct = (data) => {
  return {
    type: 'ADD_PRODUCT',
     id: productId++,
    data
  }
}

export const deleteProduct = (data) => {
  return {
    type: 'DELETE_PRODUCT',
    data
  }
}
export const updateProduct = (data) => {
  return {
    type: 'UPDATE_PRODUCT',
    data:data
  }
}

//reducer
const product = (state = [], action) => {
  switch (action.type) {

	case 'ADD_PRODUCT':
          let id=action.id;
		 return [...state, {name:action.data.name,price:action.data.price,stock:action.data.stock,id:action.id}]
	   case 'DELETE_PRODUCT':
      let index = _.findIndex(state, {id:action.data.id });
      state.splice(index,1)
      return [...state];
    case 'UPDATE_PRODUCT':
      let productIndex = _.findIndex(state, {id:action.data.id });
            state[productIndex].name =action.data.name;
            state[productIndex].price =action.data.price; 
            state[productIndex].stock =action.data.stock;     
            return [...state];

    default:
      return state
  }
}

export default product