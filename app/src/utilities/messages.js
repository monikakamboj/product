/*
 * @file: messages.js
 * @description: Handle success and error messages for the application 
 * @date: 8.6.2018
 * @author: Monika Rani
 * 
 */

const Message = {
 	productNameEmpty: "*Please enter product name",
 	priceEmpty		: "*Please enter product price",
 	stockEmpty		: "*Please enter product stock",
 	deleteWarning   : "You will not be able to recover this data!",
 	confirmDelete   : "Do you really want to delete?",
 	message         : "Product delete successfully",
 	updateDetail    : "Product detail updated successfully",
 	addDetail       : "Product detail Add successfully",

}


module.exports = Message;