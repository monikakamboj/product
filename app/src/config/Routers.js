import React from "react";
import { Route,Switch } from "react-router-dom";
import Product from '../container/Product';

const Routers = store => {
	 return (
        <div>
            <Switch> 
				<Route exact path="/" component={Product} />
			</Switch>
        </div>
    );
};

export default Routers;
